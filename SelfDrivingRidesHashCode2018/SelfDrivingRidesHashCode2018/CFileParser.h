#ifndef _CFILEPARSER_H
#define _CFILEPARSER_H

#include <iostream>
#include <string> 
#include <vector>
#include <map>

#include "ConstantsHelper.h"
using namespace NConstants;

typedef std::vector<std::vector<char>> TGrid;

class CFileParser
{
public:

	void parseFile(const std::string& fileName);
	void writeToFile(const std::string& fileName);
	void drowRides(const std::vector<sRide>& rides) const;

	void run();

	int getRowsCount() const;
	int getColumnsCount() const;
	int getVehiclesCount() const;
	int getRidesCount() const;
	int getBonusesCount() const;
	int getStepsCount() const;
	const TGrid& getGrid() const;

private:
	std::vector<int> convertStrToInt(const std::vector<std::string>& listStr);
	std::vector<std::string> splitStr(const std::string& input, const std::string& regex);

	bool toRide(std::vector<sRide>& rides, std::vector<sVehicle>& vehicles, const bool isForceSearch = false);
	int findTheBestVehicleIndex(const sRide& ride, std::vector<sVehicle>& vehicles, const bool isForceSearch = false);

	void createVehicles();
	void addRide(const sRide& ride);
	int computeDistance(const sCoord& coord1, const sCoord& coord2) const;
	eQuarterIndex getQuarterIndex(const sCoord& coord) const;
	void moveToNewQuarter(const eQuarterIndex currentQuarter, eQuarterIndex newQuarter, int vehicleIndex);

	int mRowsCount;
	int mColumnsCount;
	int mVehiclesCount;
	int mRidesCount;
	int mBonusCount;
	int mStepsCount;
	std::vector<sRide> mRides_1;
	std::vector<sRide> mRides_2;
	std::vector<sRide> mRides_3;
	std::vector<sRide> mRides_4;
	std::map<eQuarterIndex, std::vector<sVehicle>> mVehiclesGrid;
	std::vector<sVehicle> mVehicleResult;
	std::vector<sRide> mRidesResult;
	TGrid mGrid;
};

inline int CFileParser::getRowsCount() const
{
	return mRowsCount;
}

inline int CFileParser::getColumnsCount() const
{
	return mColumnsCount;
}

inline int CFileParser::getVehiclesCount() const
{
	return mVehiclesCount;
}

inline int CFileParser::getRidesCount() const
{
	return mRidesCount;
}

inline int CFileParser::getStepsCount() const
{
	return mStepsCount;
}

inline int CFileParser::getBonusesCount() const
{
	return mBonusCount;
}

#endif //_CFILEPARSER_H