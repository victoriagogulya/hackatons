#ifndef _CONSTANTSHELPER_H
#define _CONSTANTSHELPER_H

#include <cstdint>
#include <vector>
using namespace std;

namespace NConstants
{
	enum eInputValuesIndexes
	{
		INDEX_ROWS,
		INDEX_COLUMNS,
		INDEX_VEHICLES,
		INDEX_RIDES,
		INDEX_BONUSES,
		INDEX_STEPS
	};

	enum eQuarterIndex
	{
		QUARTER_1,
		QUARTER_2,
		QUARTER_3,
		QUARTER_4
	};

	struct sCoord
	{
		int mX;
		int mY;

		sCoord() : mY(0), mX(0) { }
	};

	struct sSize
	{
		int mHeight;
		int mWidth;
	};

	struct sRect
	{
		sCoord mLeftUp;
		sCoord mRightBottom;
	};

	struct sRide
	{
		sCoord  mPosStart;
		sCoord  mPosFinish;
		int     mStepsStart;
		int     mStepsFinish;
		int     mID;

		sRide() : mID(-1) { }
	};

	struct sVehicle
	{
		sCoord        mCurrentPos;
		vector<int>   mRidesCompleted;
		int64_t       mStepsToRideDifference;
		int           mID;
		int64_t       mTotalsSteps;

		sVehicle() : mStepsToRideDifference(0), mID(-1), mTotalsSteps(0) {}
	};
}

#endif //_CONSTANTSHELPER_H