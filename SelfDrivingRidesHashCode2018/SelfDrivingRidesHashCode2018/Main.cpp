#include <iostream>
#include <ctime> 

#include "CFileParser.h"
#include "ConstantsHelper.h"

using namespace std;


void main()
{
	string str_1 = "a_example.in";
	string str_2 = "b_should_be_easy.in";
	string str_3 = "c_no_hurry.in";
	string str_4 = "d_metropolis.in";
	string str_5 = "e_high_bonus.in";
	string str_6 = "test.in";

	CFileParser parser;
	parser.parseFile(str_5);

	unsigned int start_time = clock();

	std::cout << "getRowsCount " << parser.getRowsCount() << std::endl;
	std::cout << "getColumnsCount " << parser.getColumnsCount() << std::endl;
	std::cout << "getVehiclesCount " << parser.getVehiclesCount() << std::endl;
	std::cout << "getRidesCount " << parser.getRidesCount() << std::endl;
	std::cout << "getBonusesCount " << parser.getBonusesCount() << std::endl;
	std::cout << "getStepsCount " << parser.getStepsCount() << std::endl;

	parser.run();

	parser.writeToFile("submission5.in");

	unsigned int end_time = clock();
	unsigned int search_time = end_time - start_time;
	cout << "runtime = " << search_time / 1000.0 << endl;
}