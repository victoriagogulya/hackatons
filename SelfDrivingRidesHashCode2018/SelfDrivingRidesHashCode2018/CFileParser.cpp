#include "CFileParser.h"
#include "ConstantsHelper.h"

#include <fstream>
#include <regex>
#include <stdlib.h>     /* abs */


void CFileParser::parseFile(const std::string& fileName)
{
	std::string str;
	std::ifstream file(fileName);

	getline(file, str);
	auto listStrings = splitStr(str, " ");
	auto listInts = convertStrToInt(listStrings);

	mRowsCount = listInts[NConstants::INDEX_ROWS];
	mColumnsCount = listInts[NConstants::INDEX_COLUMNS];
	mVehiclesCount = listInts[NConstants::INDEX_VEHICLES];
	mRidesCount = listInts[NConstants::INDEX_RIDES];
	mBonusCount = listInts[NConstants::INDEX_BONUSES];
	mStepsCount = listInts[NConstants::INDEX_STEPS];

	createVehicles();

	int rideIndex = 0;

	while (getline(file, str))
	{
		listStrings = splitStr(str, " ");
		listInts = convertStrToInt(listStrings);
		sRide ride;
		ride.mPosStart.mY = listInts[0];
		ride.mPosStart.mX = listInts[1];
		ride.mPosFinish.mY = listInts[2];
		ride.mPosFinish.mX = listInts[3];
		ride.mStepsStart = listInts[4];
		ride.mStepsFinish = listInts[5];
		ride.mID = rideIndex;

		addRide(ride);

		rideIndex++;
	}

	file.close();
}

void CFileParser::addRide(const sRide& ride)
{
	auto index = getQuarterIndex(ride.mPosStart);

	switch (index)
	{
	case eQuarterIndex::QUARTER_1:
		mRides_1.push_back(ride);
		break;
	case eQuarterIndex::QUARTER_2:
		mRides_2.push_back(ride);
		break;
	case eQuarterIndex::QUARTER_3:
		mRides_3.push_back(ride);
		break;
	case eQuarterIndex::QUARTER_4:
		mRides_4.push_back(ride);
		break;
	}
}

eQuarterIndex CFileParser::getQuarterIndex(const sCoord& coord) const
{
	eQuarterIndex index = eQuarterIndex::QUARTER_1;

	if (coord.mY < mRowsCount / 2) // 1 or 2
	{
		index = (coord.mX < mColumnsCount) ? eQuarterIndex::QUARTER_1 : eQuarterIndex::QUARTER_2;
	}
	else // 3 or 4
	{
		index = (coord.mX < mColumnsCount) ? eQuarterIndex::QUARTER_3 : eQuarterIndex::QUARTER_4;
	}

	return index;
}

void CFileParser::writeToFile(const std::string& fileName)
{
	std::ofstream out;
	//cout << "SIZE=" << mVehicleResult.size() << endl;

	out.open(fileName);

	for (int i = 0; i < mVehicleResult.size(); i++)
	{
		out << mVehicleResult[i].mRidesCompleted.size() << ' ';

		for (auto rideIndex : mVehicleResult[i].mRidesCompleted)
		{
			out << rideIndex << ' ';
		}
		out << endl;
	}

	out.close();
}

int CFileParser::computeDistance(const sCoord& coord1, const sCoord& coord2) const
{
	const int distance = abs(coord1.mY - coord2.mY) + abs(coord1.mX - coord2.mX);
	//cout << "computeWay() - distance=" << distance << endl;
	return distance;
}

int CFileParser::findTheBestVehicleIndex(const sRide& ride, std::vector<sVehicle>& vehicles, const bool isForceSearch)
{
	int64_t minDistanceDifference = INT64_MAX;
	int index = -1;
	int counter = 0;

	for (auto& veh : vehicles)
	{
		const int distance = computeDistance(ride.mPosStart, veh.mCurrentPos);
		veh.mStepsToRideDifference = distance - ride.mStepsStart;

		int stepsForRide = computeDistance(veh.mCurrentPos, ride.mPosFinish);
		bool couldMove = veh.mTotalsSteps + stepsForRide <= ride.mStepsStart + ride.mStepsFinish;

		if (couldMove && veh.mStepsToRideDifference < minDistanceDifference)
		{
			minDistanceDifference = veh.mStepsToRideDifference;
			index = counter;
			if (veh.mStepsToRideDifference <= 0)
			{
				break;
			}
		}
		counter++;
	}


	if (index == -1 && isForceSearch)
	{
		counter = 0;
		minDistanceDifference = INT64_MAX;

		for (auto& veh : vehicles)
		{
			if (veh.mStepsToRideDifference < minDistanceDifference)
			{
				minDistanceDifference = veh.mStepsToRideDifference;
				index = counter;
			}
			counter++;
		}
	}

	return index;
}

bool CFileParser::toRide(std::vector<sRide>& rides, std::vector<sVehicle>& vehicles, const bool isForceSearch)
{
	int64_t minDistanceDifference = INT64_MAX;
	int indexVehicleBest = -1;
	int indexRideBest = -1;
	int counter = 0;

	int index = -1;
	for (auto& ride : rides)
	{
		index = findTheBestVehicleIndex(ride, vehicles, isForceSearch);

		if (index != -1 && vehicles[index].mStepsToRideDifference < minDistanceDifference)
		{
			minDistanceDifference = vehicles[index].mStepsToRideDifference;
			indexVehicleBest = index;
			indexRideBest = counter;

			if (vehicles[index].mStepsToRideDifference <= 0)
			{
				break;
			}
		}

		counter++;
	}

	if (index != -1)
	{
		auto currentQuarter = getQuarterIndex(vehicles[indexVehicleBest].mCurrentPos);

		vehicles[indexVehicleBest].mTotalsSteps += computeDistance(vehicles[indexVehicleBest].mCurrentPos,
			                                       rides[indexRideBest].mPosFinish) +
			                                       rides[indexRideBest].mStepsStart;
		vehicles[indexVehicleBest].mCurrentPos.mY = rides[indexRideBest].mPosFinish.mY;
		vehicles[indexVehicleBest].mCurrentPos.mX = rides[indexRideBest].mPosFinish.mX;
		vehicles[indexVehicleBest].mRidesCompleted.push_back(rides[indexRideBest].mID);

		auto newQuarter = getQuarterIndex(vehicles[indexVehicleBest].mCurrentPos);
		
		if (!isForceSearch && (currentQuarter != newQuarter))
		{
			moveToNewQuarter(currentQuarter, newQuarter, indexVehicleBest);
		}

		/*cout << "toRide() - vehicleID=" << mVehicles[indexVehicleBest].mID << ' '
		<< "rideID=" << mRides[indexRideBest].mID << endl;*/

		rides.erase(rides.begin() + indexRideBest);
		cout << rides.size() << endl;
	}

	return (index != -1);
}

void CFileParser::moveToNewQuarter(const eQuarterIndex currentQuarter, eQuarterIndex newQuarter, int vehicleIndex)
{
	mVehiclesGrid[newQuarter].push_back(mVehiclesGrid[currentQuarter][vehicleIndex]);

	mVehiclesGrid[currentQuarter].erase(mVehiclesGrid[currentQuarter].begin() + vehicleIndex);
}

void CFileParser::run()
{
	auto lambda = [](const sRide& ride1, const sRide& ride2)
	{
		return ride1.mStepsFinish < ride2.mStepsFinish;
	};

	std::sort(mRides_1.begin(), mRides_1.end(), lambda);
	std::sort(mRides_2.begin(), mRides_2.end(), lambda);
	std::sort(mRides_3.begin(), mRides_3.end(), lambda);
	std::sort(mRides_4.begin(), mRides_4.end(), lambda);

	bool toRide_1 = true;
	bool toRide_2 = true;
	bool toRide_3 = true;
	bool toRide_4 = true;
	bool toRideAll = true;
	bool ridesExists = true;

	while (ridesExists && toRideAll)
	{
		toRide_1 = toRide(mRides_1, mVehiclesGrid[eQuarterIndex::QUARTER_1]);
		toRide_2 = toRide(mRides_2, mVehiclesGrid[eQuarterIndex::QUARTER_2]);
		toRide_3 = toRide(mRides_3, mVehiclesGrid[eQuarterIndex::QUARTER_3]);
		toRide_4 = toRide(mRides_4, mVehiclesGrid[eQuarterIndex::QUARTER_4]);

		toRideAll = (toRide_1 || toRide_2 || toRide_3 || toRide_4);
		ridesExists = (!mRides_1.empty() || !mRides_2.empty() || !mRides_3.empty() || !mRides_4.empty());
	}

	mVehicleResult.insert(mVehicleResult.end(), mVehiclesGrid[eQuarterIndex::QUARTER_1].begin(), mVehiclesGrid[eQuarterIndex::QUARTER_1].end());
	mVehicleResult.insert(mVehicleResult.end(), mVehiclesGrid[eQuarterIndex::QUARTER_2].begin(), mVehiclesGrid[eQuarterIndex::QUARTER_2].end());
	mVehicleResult.insert(mVehicleResult.end(), mVehiclesGrid[eQuarterIndex::QUARTER_3].begin(), mVehiclesGrid[eQuarterIndex::QUARTER_3].end());
	mVehicleResult.insert(mVehicleResult.end(), mVehiclesGrid[eQuarterIndex::QUARTER_4].begin(), mVehiclesGrid[eQuarterIndex::QUARTER_4].end());

	mRidesResult.insert(mRidesResult.end(), mRides_1.begin(), mRides_1.end());
	cout << "mRidesResult.size=" << mRidesResult.size() << endl;
	mRidesResult.insert(mRidesResult.end(), mRides_2.begin(), mRides_2.end());
	cout << "mRidesResult.size=" << mRidesResult.size() << endl;
	mRidesResult.insert(mRidesResult.end(), mRides_3.begin(), mRides_3.end());
	cout << "mRidesResult.size=" << mRidesResult.size() << endl;
	mRidesResult.insert(mRidesResult.end(), mRides_4.begin(), mRides_4.end());
	cout << "mRidesResult.size=" << mRidesResult.size() << endl;

	if (ridesExists)
	{
		while (!mRidesResult.empty())
		{
			toRide(mRidesResult, mVehicleResult, true);
		}
	}

}

void CFileParser::createVehicles()
{
	mVehiclesGrid[eQuarterIndex::QUARTER_1] = std::vector<sVehicle>();
	mVehiclesGrid[eQuarterIndex::QUARTER_2] = std::vector<sVehicle>();
	mVehiclesGrid[eQuarterIndex::QUARTER_3] = std::vector<sVehicle>();
	mVehiclesGrid[eQuarterIndex::QUARTER_4] = std::vector<sVehicle>();

	for (int i = 0; i < mVehiclesCount; i++)
	{
		sVehicle vehicle;
		vehicle.mID = i + 1;
		mVehiclesGrid[eQuarterIndex::QUARTER_1].push_back(vehicle);
	}
}

void CFileParser::drowRides(const std::vector<sRide>& rides) const
{
	for (auto ride : rides)
	{
		std::cout << ride.mPosStart.mY << ' ' << ride.mPosStart.mX << ' '
			<< ride.mPosFinish.mY << ' ' << ride.mPosFinish.mX << ' '
			<< ride.mStepsStart << ' ' << ride.mStepsFinish << std::endl;
	}
}

std::vector<int> CFileParser::convertStrToInt(const std::vector<std::string>& listStr)
{
	std::vector<int> outputList;

	for (auto symbol : listStr)
	{
		outputList.push_back(atoi(symbol.c_str()));
	}

	return outputList;
}

std::vector<std::string> CFileParser::splitStr(const std::string& input, const std::string& regex)
{
	std::regex re(regex);
	std::sregex_token_iterator first{ input.begin(), input.end(), re, -1 }, last;

	return{ first, last };
}